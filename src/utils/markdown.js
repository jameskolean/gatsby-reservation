import React from 'react'
import ReactMarkdown from 'markdown-to-jsx'
import { withStyles } from '@material-ui/core/styles'
import Typography from '@material-ui/core/Typography'
import { Link } from 'gatsby-theme-material-ui'

const styles = (theme) => ({
  listItem: {
    marginTop: theme.spacing(1),
  },
})
const MyBackQuotes = ({ children, ...props }) => {
  return <Typography {...props}>{children[0].props.children}</Typography>
}
const options = {
  overrides: {
    h1: {
      component: Typography,
      props: {
        gutterBottom: true,
        variant: 'h5',
      },
    },
    h2: { component: Typography, props: { gutterBottom: true, variant: 'h6' } },
    h3: {
      component: Typography,
      props: { gutterBottom: true, variant: 'subtitle1' },
    },
    h4: {
      component: Typography,
      props: { gutterBottom: true, variant: 'caption', paragraph: true },
    },
    gutterBottom: true,
    variant: 'body2',
    color: 'textSecondary',
    component: 'p',

    p: {
      component: Typography,
      props: {
        gutterBottom: true,
        variant: 'body2',
        color: 'textSecondary',
        component: 'p',
      },
    },

    blockquote: {
      component: MyBackQuotes,
      props: {
        gutterBottom: true,
        variant: 'caption',
        color: 'textSecondary',
        component: 'p',
      },
    },

    a: { component: Link },
    li: {
      component: withStyles(styles)(({ classes, ...props }) => (
        <li className={classes.listItem}>
          <Typography component='span' {...props} />
        </li>
      )),
    },
  },
}

export default function Markdown(props) {
  return <ReactMarkdown options={options} {...props} />
}
