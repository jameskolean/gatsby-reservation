import React from 'react'
import { ApolloProvider } from '@apollo/react-hooks'
import ApolloClient from 'apollo-boost'
import fetch from 'isomorphic-fetch'

import { getIdToken, getHasuraRole } from './auth'

const client = new ApolloClient({
  uri: process.env.GATSBY_HASURA_URL,
  fetch,
  request: (operation) => {
    const token = getIdToken()
    operation.setContext({
      headers: {
        authorization: token ? `Bearer ${token}` : '',
        'x-hasura-role': getHasuraRole(),
      },
    })
  },
})

const ApolloRootElementWrapper = ({ element }) => (
  <ApolloProvider client={client}>{element}</ApolloProvider>
)

export default ApolloRootElementWrapper
