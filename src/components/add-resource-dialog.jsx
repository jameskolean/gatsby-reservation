import React from 'react'
import {
  Dialog,
  DialogActions,
  DialogContent,
  DialogContentText,
  DialogTitle,
} from '@material-ui/core'
import { Button, IconButton } from 'gatsby-theme-material-ui'
import { FormControl, Grid, TextField, Tooltip } from '@material-ui/core'
import AddIcon from '@material-ui/icons/Add'

const AddResourceDialog = ({ open, handleCancle, handleSubmit }) => {
  const [resourceName, setResourceName] = React.useState()
  const [days, setDays] = React.useState([{ day: '', hour: '', quantity: '' }])
  const [dayIndexes, setDayIndexes] = React.useState([0])
  const [dayHasError, setDayHasError] = React.useState([false])
  const [hourHasError, setHourHasError] = React.useState([false])
  const handleAddDay = () => {
    setDays((oldDays) => [...oldDays, { day: '', hour: '', quantity: '' }])
    setDayIndexes((old) => [...old, old.length])
    setDayHasError((old) => [...old, false])
    setHourHasError((old) => [...old, false])
  }
  const validateDay = (index) => {
    let re = new RegExp(
      '(((sun)|(mon)|(tues)|(wed)|(thurs)|(fri)|(sat))-((sun)|(mon)|(tues)|(wed)|(thurs)|(fri)|(sat)))|((sun)|(mon)|(tues)|(wed)|(thurs)|(fri)|(sat))'
    )
    const val = [...dayHasError]
    val[index] = !re.test(days[index].day)
    setDayHasError(val)
  }
  const validateHour = (index) => {
    const val = [...hourHasError]
    let parts = days[index].hour.split('-')
    if (parts.length === 1) {
      const num = Number(parts[0])
      val[index] = !(0 <= num && 23 >= num)
    } else if (parts.length === 2) {
      const val1 = Number(parts[0])
      const val2 = Number(parts[1])
      const val1Bad = !(0 <= val1 && 23 >= val1)
      const val2Bad = !(0 <= val2 && 23 >= val2)
      val[index] = val1Bad || val2Bad || val1 >= val2
    } else {
      val[index] = true
    }
    setHourHasError(val)
  }
  const handleDayChange = (index, value) => {
    setDays((old) => {
      const before = days.slice(0, index)
      const after = days.slice(index + 1)
      return []
        .concat(before)
        .concat([{ ...days[index], day: value }])
        .concat(after)
    })
  }
  const handleHourChange = (index, value) => {
    setDays((old) => {
      const before = days.slice(0, index)
      const after = days.slice(index + 1)
      return []
        .concat(before)
        .concat([{ ...days[index], hour: value }])
        .concat(after)
    })
  }
  const handleQuantityChange = (index, value) => {
    setDays((old) => {
      const before = days.slice(0, index)
      const after = days.slice(index + 1)
      return []
        .concat(before)
        .concat([{ ...days[index], quantity: value }])
        .concat(after)
    })
  }
  return (
    <Dialog
      open={open}
      onClose={handleCancle}
      aria-labelledby='form-dialog-title'
    >
      <DialogTitle id='form-dialog-title'>Create Reservable</DialogTitle>
      <DialogContent>
        <DialogContentText>
          Add something you want to reserve.
        </DialogContentText>
        <FormControl fullWidth>
          <TextField
            id='recource-name'
            label='Name'
            style={{ margin: 8 }}
            value={resourceName}
            onChange={(e) => setResourceName(e.currentTarget.value)}
            //            placeholder='Placeholder'
            //            helperText='Full width!'
            //            fullWidth
            margin='normal'
            InputLabelProps={{
              shrink: true,
            }}
          />
        </FormControl>
        {dayIndexes.map((dayIndex) => (
          <Grid container>
            <Grid item xs={4}>
              <FormControl>
                <TextField
                  error={dayHasError[dayIndex]}
                  helperText='Valid values sun, mon, tues, wed, thurs, fri, sat or a range like mon-fri'
                  onBlur={() => validateDay(dayIndex)}
                  id={`day-${dayIndex}`}
                  label='days'
                  style={{ margin: 8 }}
                  value={days[dayIndex].day}
                  onChange={(e) =>
                    handleDayChange(dayIndex, e.currentTarget.value)
                  }
                  //            placeholder='Placeholder'
                  //            helperText='Full width!'
                  //           fullWidth
                  margin='normal'
                  InputLabelProps={{
                    shrink: true,
                  }}
                />
              </FormControl>
            </Grid>
            <Grid item xs={4}>
              <FormControl>
                <TextField
                  error={hourHasError[dayIndex]}
                  helperText='Valid values 0 to 23 or a range like 10-15'
                  onBlur={() => validateHour(dayIndex)}
                  id={`hour-${dayIndex}`}
                  label='hours'
                  style={{ margin: 8 }}
                  value={days[dayIndex].hour}
                  onChange={(e) =>
                    handleHourChange(dayIndex, e.currentTarget.value)
                  }
                  //            placeholder='Placeholder'
                  //            helperText='Full width!'
                  //           fullWidth
                  margin='normal'
                  InputLabelProps={{
                    shrink: true,
                  }}
                />
              </FormControl>
            </Grid>
            <Grid item xs={4}>
              <FormControl>
                <TextField
                  id={`quantity-${dayIndex}`}
                  label='Quantity'
                  style={{ margin: 8 }}
                  value={days[dayIndex].quantity}
                  onChange={(e) =>
                    handleQuantityChange(dayIndex, e.currentTarget.value)
                  }
                  //            placeholder='Placeholder'
                  //            helperText='Full width!'
                  //           fullWidth
                  margin='normal'
                  InputLabelProps={{
                    shrink: true,
                  }}
                />
              </FormControl>
            </Grid>
          </Grid>
        ))}
      </DialogContent>
      <DialogContent>
        <IconButton
          aria-label='add day'
          size='small'
          onClick={() => handleAddDay()}
        >
          <Tooltip
            placement='top-start'
            title='Add a Doy of Operation'
            aria-label='Add a Doy of Operation'
          >
            <AddIcon />
          </Tooltip>
        </IconButton>
      </DialogContent>
      <DialogActions>
        <Button onClick={handleCancle} color='primary'>
          Cancel
        </Button>
        <Button
          onClick={() =>
            handleSubmit({
              name: resourceName,
              days: days,
            })
          }
          color='primary'
          //          disabled={!resourceName}
        >
          Reserve
        </Button>
      </DialogActions>
    </Dialog>
  )
}
export default AddResourceDialog
