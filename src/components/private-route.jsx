import React from 'react'
import { isAuthenticated, login } from '../utils/auth'
const PrivateRoute = ({ component: Component, location, ...rest }) => {
  if (!isAuthenticated()) {
    login()
    return null
  }
  return <Component {...rest} />
}
export default PrivateRoute
