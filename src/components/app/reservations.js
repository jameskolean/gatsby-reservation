import React from 'react'
import { useQuery } from '@apollo/react-hooks'
import gql from 'graphql-tag'
import { useMutation } from '@apollo/react-hooks'
import moment from 'moment'
import { Box, Grid, Tooltip, Typography } from '@material-ui/core'
import { getAccessToken, getHasuraUserId } from '../../utils/auth'
import { IconButton } from 'gatsby-theme-material-ui'
import SEO from '../seo'
import PinnedSubheaderList from '../pinned-subheader-list'
import AddIcon from '@material-ui/icons/Add'
import CreateIcon from '@material-ui/icons/Create'
import AddReservationDialog from '../add-reservation-dialog'
import AddRecourceDialog from '../add-resource-dialog'

const Content = () => {
  const [openAddScheduleDialog, setOpenAddScheduleDialog] = React.useState(
    false
  )
  const [openAddResourceDialog, setOpenAddResourceDialog] = React.useState(
    false
  )

  const [addResourceMutation] = useMutation(APOLLO_ADD_RESOURCE_MUTATION)
  const [deleteResourceMutation] = useMutation(APOLLO_DELETE_RESOURCE_MUTATION)
  const [makeReservationMutation] = useMutation(APOLLO_MAKE_RESERVTION_MUTATION)
  const [deleteReservationMutation] = useMutation(
    APOLLO_DELETE_RESERVTION_MUTATION
  )

  const { data, loading, error, refetch } = useQuery(APOLLO_RESERVATION_QUERY, {
    variables: { authId: getHasuraUserId() },
    context: { headers: { Authorization: 'Bearer ' + getAccessToken() } },
  })
  if (error)
    return (
      <Grid container spacing={1}>
        <Grid item xs={12}>
          <Typography variant='body1'>
            Something when wrong and I can't load your reservations
          </Typography>
          <Typography variant='body1'>
            {JSON.stringify(error, null, 2)}
          </Typography>
        </Grid>
      </Grid>
    )
  if (loading)
    return (
      <Grid container spacing={1}>
        <Grid item xs={12}>
          <Typography variant='body1'>loaging ...</Typography>
        </Grid>
      </Grid>
    )
  const formatDate = (day, hour) => {
    const when = moment(day, 'YYYY-MM-DD')
    when.add(hour, 'hours')
    return when.format('dddd MMM DD YYYY h a')
  }
  const listData = data.SchedulableResources.map((resource) => {
    return {
      name: resource.name,
      id: resource.id,
      items: resource.reservations.map((reservation) => {
        return {
          name: formatDate(reservation.day, reservation.hour),
          id: reservation.id,
        }
      }),
    }
  })

  const handleAddResource = ({ name, days }) => {
    const data = days.map((aDay) => {
      return { days: aDay.day, hours: aDay.hour, quantity: aDay.quantity }
    })
    addResourceMutation({
      variables: {
        name: name,
        data: data,
      },
      headers: { Authorization: 'Bearer ' + getAccessToken() },
      optimisticResponse: true,
    })
      .then((data) => {
        setOpenAddResourceDialog(false)
        refetch()
      })
      .catch((err) => {
        console.log('error', err)
      })
  }

  const handleDeleteResource = (id) => {
    deleteResourceMutation({
      variables: {
        id: id,
      },
      headers: { Authorization: 'Bearer ' + getAccessToken() },
      optimisticResponse: true,
    })
      .then((data) => {
        refetch()
      })
      .catch((err) => {
        console.log('error', err)
      })
  }

  const handleMakeReservation = ({ resourceId, hour, day }) => {
    makeReservationMutation({
      variables: {
        resourceId: resourceId,
        hour: hour,
        day: day,
        userId: getHasuraUserId(),
      },
      headers: { Authorization: 'Bearer ' + getAccessToken() },
      optimisticResponse: true,
    })
      .then((data) => {
        setOpenAddScheduleDialog(false)
        refetch()
      })
      .catch((err) => {
        console.log('error', err)
      })
  }

  const handleDeleteReservation = (id) => {
    deleteReservationMutation({
      variables: {
        id: id,
      },
      headers: { Authorization: 'Bearer ' + getAccessToken() },
      optimisticResponse: true,
    })
      .then((data) => {
        refetch()
      })
      .catch((err) => {
        console.log('error', err)
      })
  }

  return (
    <>
      <Grid container spacing={1}>
        <Grid item xs={12} style={{ display: 'flex' }}>
          <Typography variant='h5'>Reservations</Typography>
          <Box style={{ flexGrow: '1' }} />
          <IconButton
            aria-label='expand row'
            size='small'
            onClick={() => setOpenAddResourceDialog(true)}
          >
            <Tooltip
              placement='top-start'
              title='Create Resource'
              aria-label='create resource'
            >
              <CreateIcon />
            </Tooltip>
          </IconButton>
          <IconButton
            aria-label='expand row'
            size='small'
            onClick={() => setOpenAddScheduleDialog(true)}
          >
            <Tooltip
              placement='top-start'
              title='Make Reservation'
              aria-label='make reservation'
            >
              <AddIcon />
            </Tooltip>
          </IconButton>
        </Grid>
        <Grid item xs={12}>
          <PinnedSubheaderList
            data={listData}
            handleDeleteReservation={handleDeleteReservation}
            handleDeleteResource={handleDeleteResource}
          />
        </Grid>
      </Grid>
      <AddReservationDialog
        open={openAddScheduleDialog}
        handleCancle={() => setOpenAddScheduleDialog(false)}
        handleSubmit={handleMakeReservation}
        schedulableResources={data.SchedulableResources}
      />
      <AddRecourceDialog
        open={openAddResourceDialog}
        handleCancle={() => setOpenAddResourceDialog(false)}
        handleSubmit={handleAddResource}
      />
    </>
  )
}
const ReservationPage = () => {
  return (
    <>
      <SEO title='Reservations' />
      <Content />
    </>
  )
}

export default ReservationPage

const APOLLO_RESERVATION_QUERY = gql`
  query reservationQuery($authId: String!) {
    SchedulableResources {
      id
      name
      hours {
        days
        hours
        quantity
      }
      reservations(where: { userId: { _eq: $authId } }) {
        id
        name
        hour
        day
        userId
      }
    }
  }
`

const APOLLO_ADD_RESOURCE_MUTATION = gql`
  mutation AddResourceMutation(
    $name: String!
    $data: [SchedulableResourceHours_insert_input!]! = {
      days: ""
      hours: ""
      quantity: 1
    }
  ) {
    insert_SchedulableResources_one(
      object: { hours: { data: $data }, name: $name }
    ) {
      id
    }
  }
`

const APOLLO_DELETE_RESOURCE_MUTATION = gql`
  mutation DeleteResourceMutation($id: uuid!) {
    delete_SchedulableResources_by_pk(id: $id) {
      id
    }
  }
`

const APOLLO_MAKE_RESERVTION_MUTATION = gql`
  mutation MakeReservationMutation(
    $day: date!
    $hour: Int!
    $userId: String!
    $resourceId: uuid!
  ) {
    insert_Reservations_one(
      object: {
        day: $day
        hour: $hour
        name: "unknown"
        userId: $userId
        resourceId: $resourceId
      }
    ) {
      id
    }
  }
`

const APOLLO_DELETE_RESERVTION_MUTATION = gql`
  mutation DeleteReservationMutation($id: uuid!) {
    delete_Reservations_by_pk(id: $id) {
      id
    }
  }
`
