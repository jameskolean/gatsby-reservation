import React from 'react'
import Box from '@material-ui/core/Box'
import Divider from '@material-ui/core/Divider'
import Typograghy from '@material-ui/core/Typography'
const Footer = () => (
  <footer>
    <Box m={3}>
      <Divider />
      <Typograghy variant='caption'>
        © {new Date().getFullYear()}, Code Green LLC
      </Typograghy>
    </Box>
  </footer>
)

export default Footer
