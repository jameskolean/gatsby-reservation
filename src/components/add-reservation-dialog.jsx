import React from 'react'
import Dialog from '@material-ui/core/Dialog'
import DialogActions from '@material-ui/core/DialogActions'
import DialogContent from '@material-ui/core/DialogContent'
import DialogContentText from '@material-ui/core/DialogContentText'
import DialogTitle from '@material-ui/core/DialogTitle'
import { Button } from 'gatsby-theme-material-ui'
import InputLabel from '@material-ui/core/InputLabel'
import Input from '@material-ui/core/Input'
import FormControl from '@material-ui/core/FormControl'
import Select from '@material-ui/core/Select'
import { DatePicker } from '@material-ui/pickers'

const formatHour = (hour) => {
  if (hour === 0) return '12:00 am'
  if (hour < 12) return `${hour}:00 am`
  if (hour === 12) return '12:00 pm'
  return `${hour - 12}:00 pm`
}
const daysOfWeek = ['sun', 'mon', 'tues', 'wed', 'thurs', 'fri', 'sat']
const addHours = (hoursArray, hoursSplit) => {
  if (hoursSplit.length === 1) {
    hoursArray.push(Number(hoursSplit))
  } else if (hoursSplit.length === 2) {
    const from = Number(hoursSplit[0])
    const to = Number(hoursSplit[1])
    if (from <= to) {
      let i = from
      for (i = from; i < to; i++) {
        hoursArray.push(Number(i))
      }
    }
  }
}

const AddReservationDialog = ({
  open,
  handleCancle,
  handleSubmit,
  schedulableResources,
}) => {
  const [selectedDate, setSelectedDate] = React.useState(new Date())
  const [selectedHour, setSelectedHour] = React.useState()
  const [selectedResource, setSelectedResource] = React.useState()
  const [openDays, setOpenDays] = React.useState()
  React.useEffect(() => {
    let daysOpen = {}
    schedulableResources.forEach((resource) => {
      daysOpen[resource.id] = { days: [], hours: {} }
      daysOpen[resource.id].hours[0] = []
      daysOpen[resource.id].hours[1] = []
      daysOpen[resource.id].hours[2] = []
      daysOpen[resource.id].hours[3] = []
      daysOpen[resource.id].hours[4] = []
      daysOpen[resource.id].hours[5] = []
      daysOpen[resource.id].hours[6] = []
      resource.hours.forEach((hour) => {
        const daysSplit = hour.days.split('-')
        const hoursSplit = hour.hours.split('-')
        if (daysSplit.length === 1) {
          const dayIndex = daysOfWeek.indexOf(daysSplit[0])
          daysOpen[resource.id].days.push(dayIndex)
          addHours(daysOpen[resource.id].hours[dayIndex], hoursSplit)
        } else if (daysSplit.length === 2) {
          const from = daysOfWeek.indexOf(daysSplit[0])
          const to = daysOfWeek.indexOf(daysSplit[1])
          if (from <= to) {
            let i = from
            for (i = from; i <= to; i++) {
              daysOpen[resource.id].days.push(i)
              addHours(daysOpen[resource.id].hours[i], hoursSplit)
            }
          }
        }
      })
    })
    setOpenDays(daysOpen)
  }, [schedulableResources])

  React.useEffect(() => {
    if (!selectedResource && schedulableResources) {
      setSelectedResource(schedulableResources[0].id)
    }
  }, [schedulableResources, selectedResource])

  const handleSelectedResourceChange = (event) => {
    setSelectedResource(event.target.value)
  }

  const HourOptions = () => {
    if (!openDays[selectedResource]) return <></>
    const allHoursOfOperation = openDays[selectedResource].hours
    const selectedDayIndex = selectedDate.getDay()
    const hoursForToday = allHoursOfOperation[selectedDayIndex]
    return (
      <>
        {hoursForToday.map((anHour) => (
          <option aria-label={formatHour(anHour)} key={anHour} value={anHour}>
            {formatHour(anHour)}
          </option>
        ))}
      </>
    )
  }
  const disableDays = (date) => {
    return openDays[selectedResource].days.indexOf(date.getDay()) === -1
  }

  return (
    <Dialog
      open={open}
      onClose={handleCancle}
      aria-labelledby='form-dialog-title'
    >
      <DialogTitle id='form-dialog-title'>Reserve Time</DialogTitle>
      <DialogContent>
        <DialogContentText>Choose a date and time.</DialogContentText>
        <FormControl>
          <InputLabel htmlFor='resource-pisker'>Resource</InputLabel>
          <Select
            native
            value={selectedResource}
            onChange={handleSelectedResourceChange}
            input={<Input id='resource-pisker' />}
          >
            <option aria-label='None' value='' />
            {schedulableResources.map((resource) => (
              <option value={resource.id} key={resource.id}>
                {resource.name}
              </option>
            ))}
          </Select>
        </FormControl>
        <FormControl>
          <DatePicker
            disableToolbar
            autoOk={true}
            variant='inline'
            format='MM/dd/yyyy'
            id='date-picker-inline'
            label='Date picker inline'
            value={selectedDate}
            onChange={setSelectedDate}
            minDate={new Date()}
            shouldDisableDate={disableDays}
          />
        </FormControl>
        <FormControl>
          <InputLabel htmlFor='hour-picker'>Hour</InputLabel>
          <Select
            native
            value={selectedHour}
            onChange={(event) =>
              setSelectedHour(Number(event.target.value) || '')
            }
            input={<Input id='hour-picker' />}
          >
            <option aria-label='None' value='' />
            <HourOptions />
          </Select>
        </FormControl>
      </DialogContent>
      <DialogActions>
        <Button onClick={handleCancle} color='primary'>
          Cancel
        </Button>
        <Button
          onClick={() =>
            handleSubmit({
              resourceId: selectedResource,
              day: selectedDate,
              hour: selectedHour,
            })
          }
          color='primary'
          disabled={!selectedHour}
        >
          Reserve
        </Button>
      </DialogActions>
    </Dialog>
  )
}
export default AddReservationDialog
