import React from 'react'
import { useStaticQuery, graphql } from 'gatsby'
import { navigate } from 'gatsby'
import PropTypes from 'prop-types'
import { makeStyles } from '@material-ui/core/styles'
import {
  Avatar,
  AppBar,
  Box,
  Toolbar,
  Typography,
  Menu,
  MenuItem,
} from '@material-ui/core'
import MenuIcon from '@material-ui/icons/Menu'
import { Button, IconButton, Link } from 'gatsby-theme-material-ui'

import { login, logout, isAuthenticated, getProfile } from '../utils/auth'

const User = () => {
  const [anchorEl, setAnchorEl] = React.useState(null)
  const open = Boolean(anchorEl)
  const handleMenu = (event) => {
    setAnchorEl(event.currentTarget)
  }
  const handleLogin = () => {
    login()
  }
  const handleLogout = () => {
    logout()
    handleClose()
  }
  const handleClose = () => {
    setAnchorEl(null)
  }
  if (!isAuthenticated())
    return (
      <Button color='inherit' onClick={handleLogin}>
        Login
      </Button>
    )
  return (
    <div>
      <Avatar
        alt={getProfile().nickname}
        src={getProfile().picture}
        onClick={handleMenu}
      />
      <Menu
        id='menu-appbar'
        anchorEl={anchorEl}
        anchorOrigin={{
          vertical: 'top',
          horizontal: 'right',
        }}
        keepMounted
        transformOrigin={{
          vertical: 'top',
          horizontal: 'right',
        }}
        open={open}
        onClose={handleClose}
      >
        <MenuItem onClick={handleClose}>Profile</MenuItem>
        <MenuItem onClick={handleClose}>My account</MenuItem>
        <MenuItem onClick={handleLogout}>Logout</MenuItem>
      </Menu>
    </div>
  )
}
const HambergerMenu = ({ classes }) => {
  const [anchorEl, setAnchorEl] = React.useState(null)
  const open = Boolean(anchorEl)
  const handleMenu = (event) => {
    setAnchorEl(event.currentTarget)
  }
  const handleClose = (location) => {
    if (location) navigate(location)
    setAnchorEl(null)
  }

  return (
    <>
      <IconButton
        edge='start'
        className={classes.menuButton}
        color='inherit'
        aria-label='menu'
        onClick={handleMenu}
      >
        <MenuIcon />
      </IconButton>
      <Menu
        id='simple-menu'
        anchorEl={anchorEl}
        keepMounted
        open={open}
        onClose={handleClose}
      >
        <MenuItem onClick={() => handleClose('/')}>Home</MenuItem>
        <MenuItem onClick={() => handleClose('/app/reservations')}>
          Reservations
        </MenuItem>
      </Menu>
    </>
  )
}

const Header = ({ siteTitle }) => {
  const classes = useStyles()
  const { site } = useStaticQuery(GRAPHQL_QUERY)

  return (
    <Box className={classes.root}>
      <AppBar position='static'>
        <Toolbar>
          <HambergerMenu classes={classes} />
          <Link to='/'>
            <Typography variant='h6' className={classes.title}>
              {site.siteMetadata.title}
            </Typography>
          </Link>
          <Box className={classes.expand}></Box>
          <User />
        </Toolbar>
      </AppBar>
    </Box>
  )
}
const GRAPHQL_QUERY = graphql`
  query {
    site {
      siteMetadata {
        title
      }
    }
  }
`

const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
  },
  menuButton: {
    marginRight: theme.spacing(2),
  },
  title: {
    color: 'white',
  },
  expand: {
    flexGrow: 1,
  },
}))

Header.propTypes = {
  siteTitle: PropTypes.string,
}

Header.defaultProps = {
  siteTitle: ``,
}

export default Header
