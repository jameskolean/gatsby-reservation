import React from 'react'
import { createStyles, Theme, makeStyles } from '@material-ui/core/styles'
import {
  List,
  ListItem,
  ListItemText,
  ListSubheader,
  ListItemIcon,
  Typography,
} from '@material-ui/core'
import DeleteIcon from '@material-ui/icons/Delete'

// Interface
export interface Item {
  name: string
  id: string
}
export interface SubheaderData {
  name: string
  items: Array<Item>
}
const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      width: '100%',
      //      maxWidth: 360,
      backgroundColor: theme.palette.background.paper,
      position: 'relative',
      overflow: 'auto',
      maxHeight: '60vh',
    },
    listSection: {
      backgroundColor: 'inherit',
    },
    ul: {
      backgroundColor: 'inherit',
      padding: 0,
    },
  })
)

export default function PinnedSubheaderList({
  data,
  handleDeleteReservation,
  handleDeleteResource,
}: {
  data: Array<SubheaderData>
  handleDeleteReservation: Function
}) {
  const classes = useStyles()

  return (
    <List className={classes.root} subheader={<li />}>
      {data.map((subheader, index) => (
        <li key={`section-${index}`} className={classes.listSection}>
          <ul className={classes.ul}>
            <ListSubheader>
              {/* <Typography variant='h5'>{subheader.name}</Typography>
              <ListItemIcon onClick={() => handleDeleteResource('item.id')}>
                <DeleteIcon />
              </ListItemIcon> */}
              <ListItem key={`resource-${index}`}>
                <ListItemText>
                  <Typography variant='h5'>{subheader.name}</Typography>
                </ListItemText>
                <ListItemIcon
                  onClick={() => handleDeleteResource(subheader.id)}
                >
                  <DeleteIcon />
                </ListItemIcon>
              </ListItem>
            </ListSubheader>
            {subheader.items.map((item, itemIndex) => (
              <ListItem key={`item-${index}-${itemIndex}`}>
                <ListItemText primary={item.name} />
                <ListItemIcon onClick={() => handleDeleteReservation(item.id)}>
                  <DeleteIcon />
                </ListItemIcon>
              </ListItem>
            ))}
          </ul>
        </li>
      ))}
    </List>
  )
}
