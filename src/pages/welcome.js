import React, { useState, useEffect } from 'react'
import { Link } from 'gatsby'

import Layout from '../components/layout'
import SEO from '../components/seo'
import { handleAuthentication, isAuthenticated } from '../utils/auth'

const Welcome = () => {
  const [isLoading, setIsLoading] = useState(true)
  useEffect(() => {
    if (isAuthenticated()) {
      // this is for page reload
      setIsLoading(false)
    } else {
      // normal callback
      handleAuthentication(() => setIsLoading(false))
    }
  }, [setIsLoading])
  if (isLoading) {
    return <p>Loading Profile</p>
  }
  return (
    <Layout>
      <SEO title='Welcome' />
      <h1>Hi from Welcome page</h1>
      <p>Welcome to the Welcome page</p>
      <Link to='/app/resources/'>Resources Page</Link>
    </Layout>
  )
}
export default Welcome
