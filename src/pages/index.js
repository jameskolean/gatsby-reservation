import React from 'react'
import Layout from '../components/layout'
import SEO from '../components/seo'
import { Grid, Typography } from '@material-ui/core'
import { Button } from 'gatsby-theme-material-ui'

const IndexPage = ({ data }) => (
  <Layout>
    <SEO title='Home' />
    <Grid container spacing={1}>
      <Grid item xs={12}>
        <Typography variant='h2'>Welcome to Reservation POC</Typography>
      </Grid>
      <Grid item xs={12}>
        <Button
          fullWidth
          variant='contained'
          color='primary'
          to='/reservations'
        >
          Get Started
        </Button>
      </Grid>
    </Grid>
  </Layout>
)

export default IndexPage
