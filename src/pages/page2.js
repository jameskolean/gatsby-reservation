import React from 'react'
import { Link } from 'gatsby'

import Layout from '../components/layout'
import SEO from '../components/seo'

const Page2 = () => {
  return (
    <Layout>
      <SEO title='Pge 2' />
      <h1>Hi from Welcome page</h1>
      <p>Welcome to page 2</p>
      <Link to='/app/resources/'>Resources Page</Link>
    </Layout>
  )
}
export default Page2
