import React from 'react'
import { Router } from '@reach/router'
import Layout from '../components/layout'
import Reservations from '../components/app/reservations'
import PrivateRoute from '../components/private-route'
const App = () => (
  <Layout>
    <Router>
      <PrivateRoute path='/app/reservations' component={Reservations} />
    </Router>
  </Layout>
)
export default App
