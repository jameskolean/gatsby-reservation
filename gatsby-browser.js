/**
 * Implement Gatsby's Browser APIs in this file.
 *
 * See: https://www.gatsbyjs.org/docs/browser-apis/
 */
import React, { useState, useEffect } from 'react'
import { silentAuth } from './src/utils/auth'
import ApolloRootElementWrapper from './src/utils/apollo'

import { MuiPickersUtilsProvider } from '@material-ui/pickers'

// pick a date util library
//import MomentUtils from '@date-io/moment';
import DateFnsUtils from '@date-io/date-fns'
//import LuxonUtils from '@date-io/luxon';

// Try to renew the session when the page reloads
const SessionCheck = ({ children }) => {
  const [isLoading, setIsLoading] = useState(true)
  useEffect(() => silentAuth(() => setIsLoading(false)))
  //  useEffect(() => silentAuth())
  if (isLoading) return <h1>Checking permissions ...</h1>
  return isLoading === false && <>{children}</>
}

export const wrapRootElement = ({ element }) => (
  <SessionCheck>
    <MuiPickersUtilsProvider utils={DateFnsUtils}>
      <ApolloRootElementWrapper element={element} />
    </MuiPickersUtilsProvider>
  </SessionCheck>
)
